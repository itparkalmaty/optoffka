<?php

use App\Http\Controllers\Api\Track\ShipController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/uploads',function(){
   return view('admin.upload');
})->name('uploads');
Route::get('/admin/user/uploads', function (){
    return view('admin.users');
})->name('user-excel');
Route::get('/history/track/{ship}', [ShipController::class, 'certainInfo']);
