<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('ship', 'ShipCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('paymentstatus', 'PaymentStatusCrudController');
    Route::crud('information', 'InformationCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('stage', 'StageCrudController');
    Route::crud('adminmessage', 'AdminMessageCrudController');
    Route::crud('userinfo', 'UserInfoCrudController');
}); // this should be the absolute last line of this file
