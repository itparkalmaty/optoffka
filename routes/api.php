<?php

use App\Http\Controllers\Api\Articles\ArticleController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\LogoutController;
use App\Http\Controllers\Api\Info\InformationController;
use App\Http\Controllers\Api\Notification\NotificationController;
use App\Http\Controllers\Api\Track\ShipController;
use App\Http\Controllers\Api\User\UserInfoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/auth')->group(function (){
    Route::post('/login', [LoginController::class, 'login']);

    Route::get('/articles', [ArticleController::class, 'index']);
});

Route::middleware('auth:sanctum')->group(function (){
    Route::post('/auth/logout', [LogoutController::class, 'logout']);
    Route::post('/create/track', [ShipController::class, 'creating']);
    Route::get('/delete/track/{ship}', [ShipController::class, 'deleting']);
    Route::get('/info', [InformationController::class, 'index']);
    Route::get('/show/track', [ShipController::class, 'show']);
    Route::get('/show/stage', [ShipController::class, 'stage']);
    Route::post('/user/update', [UserInfoController::class, 'update']);
    Route::get('/user/staticInfo', [UserInfoController::class, 'login']);
    Route::get('/user/notifications', [NotificationController::class, 'history']);
    Route::get('/show/track/info/{ship}', [ShipController::class, 'showCertain']);
    Route::get('/show/user/info', [UserInfoController::class, 'info']);
});

Route::post('/load', [ShipController::class, 'load']);
Route::post('/other/load', [ShipController::class, 'otherLoad']);
Route::post('/another/load', [ShipController::class, 'anotherLoad']);
Route::post('/users/load', [UserInfoController::class, 'upload']);
Route::post('test', [ShipController::class, 'test']);


Route::prefix('/admin')->group(function(){
    Route::get('/unlink/{track}', [ShipController::class, 'unlink']);
    Route::get('/link/{track}', [ShipController::class, 'link']);
});

Route::get('/test/notify', [NotificationController::class, 'cron']);
Route::get('/test/deleted', [ShipController::class, 'testDeleted']);

