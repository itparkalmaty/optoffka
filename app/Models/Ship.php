<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ship extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $dates = [
        'date_of_ship',
        'updated_at',
        'second_stage_date',
        'third_stage_date',
        'fourth_stage_date',
    ];
    protected $fillable = [
        'track_number',
        'description',
        'user_id',
        'stage_id',
        'file',
        'created_at',
        'date_of_ship',
        'user_name',
        'second_stage_date',
        'third_stage_date',
        'fourth_stage_date',
        'notification_stage'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function history($crud = false)
    {
        return "<a class='btn btn-sm btn-link' href='/history/track/" . $this->attributes['id'] . "' ><i class='la la-area-chart'></i>История</a>";
    }

}
