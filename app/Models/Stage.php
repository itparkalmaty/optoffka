<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stage extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name', 'description'
    ];

    public function ship()
    {
        return $this->hasMany(Ship::class);
    }

    public function payment()
    {
        return $this->belongsTo(PaymentStatus::class);
    }


}
