<?php

namespace App\Providers;

use App\Models\AdminMessage;
use App\Models\Ship;
use App\Models\Stage;
use App\Observers\AdminMessageObserver;
use App\Observers\ShipObserver;
use App\Observers\StageObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Ship::observe(ShipObserver::class);
        AdminMessage::observe(AdminMessageObserver::class);
    }
}
