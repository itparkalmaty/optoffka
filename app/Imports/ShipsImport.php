<?php

namespace App\Imports;

use App\Http\Controllers\Api\Track\ShipController;
use App\Models\Ship;
use App\Models\Stage;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Console\Input\Input;

class ShipsImport implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use Queueable, Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    private $file, $date, $create;

    public function chunkSize(): int
    {
        return 10;
    }

    public function __construct($file, $date)
    {
        $this->file = $file;
        $this->date = $date;
        $this->create = new ShipController();
    }

    public function model(array $row)
    {
//        if ($row['treki_usluga_sklada'] != null)
//            switch ($ship = Ship::where('track_number', $row['treki_usluga_sklada'])->first() != null) {
//                case 1:
//                    $ship = Ship::where('track_number', $row['treki_usluga_sklada'])->first();
//                    if ($ship !== null) {
//                        $ship->update([
//                            'date_of_ship' => $this->date,
//                            'stage_id' => 2,
//                            'file' => $this->file->getClientOriginalName(),
//                        ]);
//                    }
//                    break;
//                case 2:
//                    if ($ship == null) {
//                        $ship = Ship::create([
//                            'date_of_ship' => $this->date,
//                            'file' => $this->file->getClientOriginalName(),
//                            'track_number' => $row['treki_usluga_sklada'],
//                            'stage_id' => 2
//                        ]);
//                    }
//                    return $ship;
//            }
        if ($row['treki_usluga_sklada'] != null)
            if (Ship::where('track_number', $row['treki_usluga_sklada'])->first() != null) {
                $ship = Ship::where('track_number', $row['treki_usluga_sklada'])->first();
                if ($ship !== null)
                    $ship->update([
                        'date_of_ship' => $this->date,
                        'stage_id' => 2,
                        'file' => 'Отправка '.$this->date,
                        'second_stage_date' => Carbon::now(),
                        'notification_stage' => 1
                    ]);
            } else {
                if (Ship::where('track_number', $row['treki_usluga_sklada'])->first() == null)
                    return new Ship([
                        'date_of_ship' => $this->date,
                        'file' => 'Отправка '.$this->date,
                        'track_number' => $row['treki_usluga_sklada'],
                        'stage_id' => 2,
                        'second_stage_date' => Carbon::now()
                    ]);
            }
    }

    public function headingRow()
    {
        return 1;
    }


}
