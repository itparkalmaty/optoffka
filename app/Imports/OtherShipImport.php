<?php

namespace App\Imports;

use App\Models\Ship;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OtherShipImport implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use Queueable, Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    private $file, $date;

    public function chunkSize(): int
    {
        return 10;
    }

    public function __construct($file, $date)
    {
        $this->file = $file;
        $this->date = $date;
    }

    public function model(array $row)
    {
//        if ($row['treki_usluga_sklada'] != null)
//            switch ($ship = Ship::where('track_number', $row['treki_usluga_sklada'])->first() != null) {
//                case 1:
//                    $ship = Ship::where('track_number', $row['treki_usluga_sklada'])->first();
//                    if ($ship !== null) {
//                        $ship->update([
//                            'date_of_ship' => $this->date,
//                            'stage_id' => 3,
//                            'file' => $this->file->getClientOriginalName(),
//                        ]);
//                    }
//                    break;
//                case 2:
//                    if ($ship === null) {
//                        $ship = Ship::create([
//                            'date_of_ship' => $this->date,
//                            'file' => $this->file->getClientOriginalName(),
//                            'track_number' => $row['treki_usluga_sklada'],
//                            'stage_id' => 3
//                        ]);
//                    }
//                    return $ship;
//            }
        if ($row['treki_usluga_sklada'] != null)
            if (Ship::where('track_number', $row['treki_usluga_sklada'])->first() != null) {
                $ship = Ship::where('track_number', $row['treki_usluga_sklada'])->first();
                if ($ship !== null)
                    $ship->update([
                        'date_of_ship' => $this->date,
                        'stage_id' => 3,
                        'file' => 'Приход '.$this->date,
                        'third_stage_date' => Carbon::now(),
                        'notification_stage' => 1
                    ]);
            } else {
                if (Ship::where('track_number', $row['treki_usluga_sklada'])->first() == null)
                    return new Ship([
                        'date_of_ship' => $this->date,
                        'file' => 'Приход '.$this->date,
                        'track_number' => $row['treki_usluga_sklada'],
                        'stage_id' => 3,
                        'third_stage_date' => Carbon::now()
                    ]);
            }

    }

    public function headingRow()
    {
        return 1;
    }
}
