<?php

namespace App\Imports;

use App\Models\AdminMessage;
use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersImport implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithCalculatedFormulas
{
    use Queueable, Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    private $file, $date;

    public function __construct($file, $date)
    {
        $this->file = $file;
        $this->date = $date;
    }

    public function chunkSize(): int
    {
        return 120;
    }

    public function model(array $row)
    {
//        dd(8);
        $data = $row['login'].$row['nomer'];
        if ($row['login'] || $row['nomer'] != null){
            $user = User::where('login', $data)->first();
//            $userInfo = UserInfo::whereNull('date')->first();
            if ($user){
                UserInfo::create([
                    'user_id' => $user->id,
//                    'message' => 'Ваши закупки',
                    'date' => $this->date,
                    'quantity' => $row['kol_vo_trekov'].' треков',
                    'weight' => $row['ves_kg'],
                    'parish_sum' => $row['summa_za_prixod'],
                    'payment' => $row['summa_k_oplate'],
                ]);
            }
        }
    }

//    public function headings(): array
//    {
//        return array_keys($this->query()->first()->toArray());
//    }

    public function headingRow(): int
    {
        return 1;
    }
}
