<?php

namespace App\Imports;

use App\Models\AdminMessage;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AdminMessageImport implements ToModel, WithHeadingRow, WithCalculatedFormulas, ShouldQueue, WithChunkReading
{
    use Queueable, Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    private $file;

    public function __construct($file)
    {
        $this->file = $file;
    }
    public function model(array $row)
    {
        $data = $row['login'].$row['nomer'];
        if ($row['login'].$row['nomer'] =! null){
            $user = User::where('login', $data)->first();
            if ($user)
            return new AdminMessage([
//                    'user_id' => $user->id,
//                    'message' => 'Ваши закупки',
                    'quantity' => $row['kol_vo_trekov'].' треков',
                    'weight' => $row['ves_kg'],
                    'parish_sum' => $row['summa_za_prixod'],
                    'payment' => $row['summa_k_oplate'],
                ]);
        }
    }

//    public function headings(): array
//    {
//        return array_keys($this->query()->first()->toArray());
//    }

//    public function headingRow(): int
//    {
//        return 1;
//    }

    public function chunkSize(): int
    {
        return 10;
    }
}
