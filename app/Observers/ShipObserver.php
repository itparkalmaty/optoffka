<?php

namespace App\Observers;

use App\Models\Ship;
use App\Models\Stage;
use App\Services\StageService;

class ShipObserver
{
    /**
     * Handle the Ship "created" event.
     *
     * @param  \App\Models\Ship  $ship
     * @return void
     */
    public function created(Ship $ship)
    {
        //
    }

    /**
     * Handle the Ship "updated" event.
     *
     * @param  \App\Models\Ship  $ship
     * @return void
     */
    public function updated(Ship $ship)
    {
        $stageService = new StageService();

        if($ship->getOriginal('stage_id') != $ship->stage_id)
            switch ($ship->stage_id) {
                case 1:
                    $stageService->firstStage($ship);
                    break;
                case 2:
                    $stageService->secondStage($ship);
                    break;
                case 3:
                    $stageService->thirdStage($ship);
                    break;
//                case 4:
//                    $stageService->fourthStage($ship);
//                    break;
//                case 5:
//                    $stageService->fifthStage($ship);
//                    break;
            }
    }

    /**
     * Handle the Ship "deleted" event.
     *
     * @param  \App\Models\Ship  $ship
     * @return void
     */
    public function deleted(Ship $ship)
    {
        //
    }

    /**
     * Handle the Ship "restored" event.
     *
     * @param  \App\Models\Ship  $ship
     * @return void
     */
    public function restored(Ship $ship)
    {
        //
    }

    /**
     * Handle the Ship "force deleted" event.
     *
     * @param  \App\Models\Ship  $ship
     * @return void
     */
    public function forceDeleted(Ship $ship)
    {
        //
    }
}
