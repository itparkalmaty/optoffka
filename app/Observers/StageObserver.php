<?php

namespace App\Observers;

use App\Models\Stage;
use App\Services\StageService;

class StageObserver
{
    /**
     * Handle the Stage "created" event.
     *
     * @param \App\Models\Stage $stage
     * @return void
     */
    public function created(Stage $stage)
    {
        //
    }

    /**
     * Handle the Stage "updated" event.
     *
     * @param \App\Models\Stage $stage
     * @return void
     */
    public function updated(Stage $stage)
    {
//        $stageService = new StageService();
//
//        if($stage->getOriginal('stage_id'))
//        switch ($stage->id) {
//            case 1:
//                $stageService->firstStage($stage);
//                break;
//            case 2:
//                $stageService->secondStage($stage);
//                break;
//            case 3:
//                $stageService->thirdStage($stage);
//                break;
//            case 4:
//                $stageService->fourthStage($stage);
//        }
    }

    /**
     * Handle the Stage "deleted" event.
     *
     * @param \App\Models\Stage $stage
     * @return void
     */
    public function deleted(Stage $stage)
    {
        //
    }

    /**
     * Handle the Stage "restored" event.
     *
     * @param \App\Models\Stage $stage
     * @return void
     */
    public function restored(Stage $stage)
    {
        //
    }

    /**
     * Handle the Stage "force deleted" event.
     *
     * @param \App\Models\Stage $stage
     * @return void
     */
    public function forceDeleted(Stage $stage)
    {
        //
    }
}
