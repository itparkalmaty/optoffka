<?php

namespace App\Observers;

use App\Models\AdminMessage;
use App\Services\MessageService;

class AdminMessageObserver
{
    /**
     * Handle the AdminMessage "created" event.
     *
     * @param  \App\Models\AdminMessage  $adminMessage
     * @return void
     */
    public function created(AdminMessage $adminMessage)
    {
        $messageService = new MessageService();

        $messageService->message($adminMessage);
    }

    /**
     * Handle the AdminMessage "updated" event.
     *
     * @param  \App\Models\AdminMessage  $adminMessage
     * @return void
     */
    public function updated(AdminMessage $adminMessage)
    {
        //
    }

    /**
     * Handle the AdminMessage "deleted" event.
     *
     * @param  \App\Models\AdminMessage  $adminMessage
     * @return void
     */
    public function deleted(AdminMessage $adminMessage)
    {
        //
    }

    /**
     * Handle the AdminMessage "restored" event.
     *
     * @param  \App\Models\AdminMessage  $adminMessage
     * @return void
     */
    public function restored(AdminMessage $adminMessage)
    {
        //
    }

    /**
     * Handle the AdminMessage "force deleted" event.
     *
     * @param  \App\Models\AdminMessage  $adminMessage
     * @return void
     */
    public function forceDeleted(AdminMessage $adminMessage)
    {
        //
    }
}
