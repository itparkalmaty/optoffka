<?php


namespace App\Services;


use App\Models\AdminMessage;

class MessageService
{
    private $pushService;

    public function __construct()
    {
        $this->pushService = new PushService();
    }

    public function message(AdminMessage $adminMessage)
    {
        $this->pushService->anotherSend($adminMessage->user->devices(),
            'Сообщение от администратора', $adminMessage->message, $adminMessage->user->id, 'Сообщение', $adminMessage->weight, $adminMessage->quantity, $adminMessage->payment);
    }
}
