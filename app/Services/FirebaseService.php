<?php


namespace App\Services;


use Illuminate\Http\Request;

class FirebaseService
{
    protected $serverKey = '';

    public function show(Request $request)
    {
        $request->user();
    }

    public function send($request, $isAndroid = 1)
    {
        $notification = [
            "to" => $isAndroid == 1? "topics/android" : "topics/ios",
            $isAndroid == 1 ? "data" : "notification" => [
                'title' => $request->title,
                'body' => $request->body
            ]
        ];
    }
}
