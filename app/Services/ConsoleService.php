<?php


namespace App\Services;


use App\Models\Ship;
use Carbon\Carbon;

class ConsoleService
{
    public function delete_old()
    {
        $ships = Ship::where('stage_id', 4)->withTrashed()->get();
        foreach ($ships as $ship){
            $to = $ship->updated_at;
            $from = Carbon::now();
            $diff_in_month = $to->diffInMonths($from);
            if ($diff_in_month > 2){
//                dd($ship);
                $ship->forceDelete();
            }
        }
    }
}
