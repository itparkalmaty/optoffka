<?php


namespace App\Services;


use App\Models\NotificationHistory;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PushService
{
    private $serverKey = 'AAAAg3G1Kp0:APA91bE0hGuLzF4lRZCJ6jO9feRd9WzH1au-DcERkcgdY8V2nZIaXWBUNvLhuyatN-vC0jcP09lIIVsHvNdVQGnOXg_641VuyyVANX47DqXbzWHwi12nhptSsq_WBN_GxQZAhZ_leCJk';
//
    public function send($device_tokens,string $title, string $text, $id, $track)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);
        NotificationHistory::create([
            'user_id' => $id,
            'title' => $title,
            'description' => $text,
            'track_number' => $track
        ]);
//        Log::info('asd  '.$response->body());
        return $response->successful();
    }

    public function anotherSend($device_tokens,string $title, string $text, $id, $track, $weight, $quantity, $payment)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);
        NotificationHistory::create([
            'user_id' => $id,
            'title' => $title,
            'description' => $text,
            'track_number' => $track,
            'weight' => $weight,
            'quantity' => $quantity,
            'payment' => $payment
        ]);
//        Log::info('asd  '.$response->body());
        return $response->successful();
    }

}
