<?php


namespace App\Services;


use App\Models\Ship;
use App\Models\Stage;
use App\Models\User;

class StageService
{
    private $pushService;

    public function __construct()
    {
        $this->pushService = new PushService();
    }

    public function setDefaultValues($data, User $user)
    {
        $data['payment_status_id'] = 1;
        $data['user_id'] = $user->id;
        return $data;
    }

    public function firstStage(Ship $ship)
    {
        if (!isset($ship->user))
            return null;
        $this->pushService->send($ship->user->devices(), 'Статус изменён',
            'Трек номер: ' . $ship->track_number . '. Добавлен клиентом', $ship->user->id, $ship->track_number);

    }

    public function secondStage(Ship $ship)
    {
        if (!isset($ship->user))
            return null;
        $this->pushService->send($ship->user->devices(), 'Статус изменён',
            'Трек номер: ' . $ship->track_number . '. Прибыло на китайский склад, отправлено на таможню', $ship->user->id, $ship->track_number);
    }

    public function thirdStage(Ship $ship)
    {
        if (!isset($ship->user))
            return null;
        $this->pushService->send($ship->user->devices(), 'Статус изменён',
            'Трек номер: ' . $ship->track_number . '. Прибыло в Алматы, готов к выдаче/отправке', $ship->user->id, $ship->track_number);
    }

    public function fourthStage(Ship $ship)
    {
        if (!isset($ship->user))
            return null;
        $this->pushService->send($ship->user->devices(), 'Статус изменён',
            'Трек номер: ' . $ship->track_number . '. Выдано/отправлено клиенту', $ship->user->id, $ship->track_number);
    }

//    public function fifthStage(Ship $ship)
//    {
//        $this->pushService->send($ship->user->devices(), 'Статус изменён',
//            'Трек номер: '.$ship->track_number.'. Доставка завершена.', $ship->user->id, $ship->track_number);
//    }


}
