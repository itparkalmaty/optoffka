<?php


namespace App\Services;


use App\Models\User;
use App\Models\UserDevice;

class NotificationService
{
    public function registerDevice(string $token, User $user)
    {
        if (!UserDevice::where('device_token', $token)->where('user_id', $user->id)->exists()) {
            UserDevice::create([
                'device_token' => $token,
                'user_id' => $user->id
            ]);
        }
    }

    public function deleteDevice(string $device_token, User $user)
    {
        User::where('device_token', $device_token)->where('user_id', $user->id)->delete();
    }
}
