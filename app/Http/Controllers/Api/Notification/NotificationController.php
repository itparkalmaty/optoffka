<?php

namespace App\Http\Controllers\Api\Notification;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Imports\AdminMessageImport;
use App\Models\NotificationHistory;
use App\Models\User;
use App\Services\PushService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class NotificationController extends Controller
{
    private $pushService;

    public function __construct()
    {
        $this->pushService = new PushService();
    }

    public function history()
    {
        $user = Auth::user();
        $history = NotificationHistory::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        return NotificationResource::collection($history);
    }

    public function cron()
    {
        $users = User::with('ships')->get();
        foreach ($users as $user) {
            $meme = 0;
            foreach ($user->ships as $ship) {
                if ($ship->notification_stage == 1){
                    $meme++;
                    $ship->update([
                        'notification_stage' => 2
                    ]);
                }
            }
            if ($meme >= 1)
                $this->pushService->send($ship->user->devices(), 'Статус доставки',
                    'Статус трек номера изменен', $ship->user->id, null);
        }
    }

}
