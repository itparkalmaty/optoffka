<?php

namespace App\Http\Controllers\Api\Track;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrackRequest;
use App\Http\Resources\CertainShipResource;
use App\Http\Resources\ShipResource;
use App\Http\Resources\ShipStageResource;
use App\Http\Resources\ShipUpdateResource;
use App\Imports\AnotherShipImport;
use App\Imports\OtherShipImport;
use App\Imports\ShipsImport;
use App\Models\Ship;
use App\Models\User;
use App\Services\ConsoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ShipController extends Controller
{

    // fucking TrackRequest doesn't work correctly.
    // Soon I'm gonna correct this shit and get rid of validators and long $request terms
    // Idk for how long comments above are written but still dat shit ain't fixed yet

    public function link($track, Request $request)
    {
        Ship::where('track_number', $track)->first()->update([
            'user_id' => $request->user
        ]);
        return 1;
    }

    public function unlink($track)
    {
        Ship::where('track_number', $track)->first()->update([
            'user_id' => null
        ]);
        return 1;
    }

    public function creating(Request $request)
    {
        $data = $request->validate([
            'track_number' => 'required',
            'description' => 'nullable',
        ]);
        if (!Ship::where('track_number', $request->track_number)->exists()) {
            $ship = Ship::create([
                'track_number' => $request->track_number,
                'description' => $request->description,
                'stage_id' => 1,
                'user_id' => auth()->user()->id,
                'user_name' => auth()->user()->login,
            ]);
        } else {
            $ship = Ship::where('track_number', $request->track_number)->first();
            $ship->update(['user_id' => auth()->user()->id, 'description' => $request->description, 'user_name' => auth()->user()->login]);
        }
        return response([
            'ship' => $ship
        ], 200);
    }

    public function deleting(Ship $ship)
    {
        $ship->delete();
        return response(['message' => 'Трек номер успешно удален']);
    }

    public function show()
    {
        $user = Auth::user();
        $ship = Ship::where('user_id', $user->id)->orderBy('updated_at', 'desc')->get();
        return ShipResource::collection($ship);
    }

    public function stage(Request $request)
    {
        $search = $request->input('track_number');
        return ShipStageResource::collection(Ship::where('track_number', 'like', '%' . $search . '%')->orderBy('created_at', 'desc')->paginate(20));
    }

    public function showCertain(Ship $ship)
    {
        return new CertainShipResource($ship);
    }

    public function certainInfo(Ship $ship)
    {
//        dd($ship->stage);
//        $prior = $ship->with()
        return view('admin.history', compact('ship'));
    }

    public function load(Request $request)
    {
        $file = $request->file('booba')->storeAs('imports', 'laravel-imports.' . $request->file('booba')->getClientOriginalExtension());
        (new ShipsImport($file, $request->date_of_ship))->queue($file, null, \Maatwebsite\Excel\Excel::XLSX);
        return response(['message' => 'Успешно отгружено', 200]);
    }

    public function otherLoad(Request $request)
    {
        $file = $request->file('dooba')->storeAs('imports', 'laravel-imports.' . $request->file('dooba')->getClientOriginalExtension());
        (new OtherShipImport($file, $request->date_of_ship))->queue($file, null, \Maatwebsite\Excel\Excel::XLSX);
        return response(['message' => 'Успешно отгружено', 200]);
    }

    public function anotherLoad(Request $request)
    {
        $file = $request->file('pooba')->storeAs('imports', 'laravel-imports.' . $request->file('pooba')->getClientOriginalExtension());
        (new AnotherShipImport($file, $request->date_of_ship))->queue($file, null, \Maatwebsite\Excel\Excel::XLSX);
        return response(['message' => 'Успешно отгружено', 200]);
    }

    public function test(Request $request)
    {
        $file = $request->file('pooba')->storeAs('imports', 'laravel-imports.' . $request->file('pooba')->getClientOriginalExtension());
        (new AnotherShipImport($file, $request->date_of_ship))->queue($file, null, \Maatwebsite\Excel\Excel::XLSX);
        return response(['message' => 'Успешно отгружено', 200]);
    }

    public function testDeleted()
    {
        $console = new ConsoleService();
        $console->delete_old();

    }
}
