<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserLoginResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserTrackResource;
use App\Imports\UsersImport;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class UserInfoController extends Controller
{
    public function update(UserUpdateRequest $request)
    {
        $data = $request->all();
        if ($request->password == null || $request->password = ''){
            unset($data['password']);
        }
        $request->user()->update($data);

        return new UserResource($request->user());
    }


    public function login()
    {
        $user = Auth::user();
        return new UserLoginResource($user);
    }

    public function upload(Request $request)
    {
        $file = $request->file('booba')->storeAs('imports', 'laravel-imports.' . $request->file('booba')->getClientOriginalExtension());
        (new UsersImport($file, $request->date))->queue($file, null, \Maatwebsite\Excel\Excel::XLSX);
        return response(['message' => 'Успешно отгружено', 200]);
    }

    public function info()
    {
        $userInfo = UserInfo::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return UserTrackResource::collection($userInfo);
    }


}
