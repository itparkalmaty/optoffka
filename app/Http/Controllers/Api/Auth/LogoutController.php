<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
//        dd($request->user()->currentAccessToken());
        $request->user()->currentAccessToken()->delete();
        return response([
            'message'=>'Вы вышли с аккаунта'
        ], 200);
    }
}
