<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    private $notify_service;
    public function __construct()
    {
        $this->notify_service = new NotificationService();
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->validate([
            'login' => ['required'],
            'password' => ['required'],
        ]);
//        $user = User::where('login', $request->login)->first();
//        $user_id = $user->id;
        if (Auth::attempt($credentials)) {
            $user = User::where('login', $credentials['login'])->first();
            $user_id = $user->id;
            $token = $user->createToken('76767');
            $this->notify_service->registerDevice($request->device_token, $user);
            return response([
                'user' => $user_id,
                'token' => $token->plainTextToken,
                'message' => 'Вы успешно вошли в систему'
            ]);
        }else{
            return response([
                'message' => 'Неверный логин или пароль'
            ]);
        }
    }
}
