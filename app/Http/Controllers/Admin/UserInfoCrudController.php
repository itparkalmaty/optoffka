<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserInfoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class UserInfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserInfoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\UserInfo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/userinfo');
        CRUD::setEntityNameStrings('Поступление пользователя', 'Поступление пользователей');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('user')->attribute('login')->label('Логин');
        CRUD::column('quantity')->label('Кол-во треков');
        CRUD::column('weight')->label('Вес');
        CRUD::column('parish_sum')->label('Сумма за приход');
        CRUD::column('sum')->label('Сумма');
        CRUD::column('date')->label('Дата');
        CRUD::column('payment')->label('Сумма к оплате');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserInfoRequest::class);

        CRUD::field('user')->attribute('login')->label('Логин');
        CRUD::field('quantity')->label('Кол-во треков');
        CRUD::field('weight')->label('Вес');
        CRUD::field('parish_sum')->label('Сумма за приход');
        CRUD::field('date')->label('Дата');
        CRUD::field('payment')->label('Сумма к оплате');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
