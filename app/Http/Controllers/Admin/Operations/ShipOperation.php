<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ShipOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupShipRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/ship', [
            'as'        => $routeName.'.ship',
            'uses'      => $controller.'@ship',
            'operation' => 'ship',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupShipDefaults()
    {
        $this->crud->allowAccess('ship');

        $this->crud->operation('ship', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'ship', 'view', 'crud::buttons.ship');
            // $this->crud->addButton('line', 'ship', 'view', 'crud::buttons.ship');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function ship()
    {
        $this->crud->hasAccessOrFail('ship');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? 'ship '.$this->crud->entity_name;

        // load the view
        return view("crud::operations.ship", $this->data);
    }
}
