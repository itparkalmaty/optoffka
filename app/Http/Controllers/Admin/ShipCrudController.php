<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ShipRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;


/**
 * Class ShipCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShipCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Ship::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ship');
        CRUD::setEntityNameStrings('Трек номер', 'Трек номера');
        $this->crud->addFilter([ // simple filter
            'type' => 'simple',
            'name' => 'trashed',
            'label' => 'Удаленные треки'
        ],
            false,
            function () { // if the filter is active
                $this->crud->addClause('onlyTrashed');
            });
//        $this->crud->orderBy('track_number', 'asc');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addButtonFromModelFunction('line', 'return', 'history', 'beginning');
        $this->crud->addButtonFromView('bottom', 'return', 'bulk_stage', 'beginning');
//        $this->crud->addColumn([
//            'type'           => 'checkbox',
//            'name'           => 'bulk_actions',
//            'label'          => ' <input type="checkbox" class="crud_bulk_actions_main_checkbox" style="width: 16px; height: 16px;" />',
//            'priority'       => 1,
//            'searchLogic'    => false,
//            'orderable'      => false,
//            'visibleInModal' => false,
//        ])->makeFirstColumn();
//        $this->crud->enableBulkActions();
        CRUD::column('track_number')->label('Трек номер');
        CRUD::column('user')->attribute('login')->label('Пользователь');
        CRUD::column('user_name')->label('Поиск пользователя');
//        $this->crud->addColumn([
//            'name' => 'user_name',
//            'label' => 'name',
//            'searchLogic' => function ($query, $column, $searchTerm) {
//            $query->orWhere('user_name', 'like', '%'.$searchTerm.'%');
//            },
//            'orderLogic' => function ($query, $column, $columnDirection) {
//                return $query->leftJoin('ships', 'ships.user_name', '=', 'users.login')
//                    ->orderBy('ships.user_name', $columnDirection)->select('users.*');
//            },
//            'orderable' => true
//        ]);
        CRUD::column('file')->label('Файл');
        CRUD::column('stage')->attribute('description')->label('Этап доставки');
//        CRUD::column('payment_status');
        CRUD::column('description')->label('Описание');
        CRUD::column('date_of_ship')->label('Дата отгрузки');
        CRUD::column('updated_at')->label('Дата обновления');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ShipRequest::class);

        CRUD::field('track_number')->label('Трек номер');
        CRUD::field('user_id')->attribute('login')->label('Пользователь');
        CRUD::field('user_name')->label('Поиск пользователя');
        CRUD::field('file')->label('Файл');
        CRUD::field('stage')->attribute('description')->label('Этап доставки');
//        CRUD::field('payment_status');
        CRUD::field('description')->label('Описание');
        CRUD::field('date_of_ship')->label('Дата отгрузки');
        CRUD::field('created_at')->label('Дата отгрузки первого статуса');
        CRUD::field('second_stage_date')->label('Дата отгрузки второго статуса');
        CRUD::field('third_stage_date')->label('Дата отгрузки третьего статуса');
        CRUD::field('fourth_stage_date')->label('Дата отгрузки четвертого статуса');


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function bulkClone()
    {
        $this->crud->hasAccessOrFail('list');

        $entries = $this->crud->getRequest()->input('entries');
        $clonedEntries = [];

        foreach ($entries as $key => $id) {
            if ($entry = $this->crud->model->find($id)) {
                $clonedEntries[] = $entry->update(['stage_id' => 2]);
            }
        }
        return $clonedEntries;
    }

    protected function setupBulkCloneRoutes($segment, $routeName, $controller)
    {
        Route::post($segment . '/bulk-clone', [
            'as' => $routeName . '.bulkClone',
            'uses' => $controller . '@bulkClone',
            'operation' => 'bulkClone',
        ]);
    }

    protected function setupBulkCloneDefaults()
    {
        $this->crud->allowAccess('bulkClone');

        $this->crud->operation('list', function () {
            $this->crud->enableBulkActions();
            $this->crud->addButtonFromView('bottom', 'bulk_update', 'bulk_update', 'bulk_update',);

        });
    }

    public function bulkClone3()
    {
        $this->crud->hasAccessOrFail('list');

        $entries = $this->crud->getRequest()->input('entries');
        $clonedEntries = [];

        foreach ($entries as $key => $id) {
            if ($entry = $this->crud->model->find($id)) {
                $clonedEntries[] = $entry->update(['stage_id' => 3]);
            }
        }
        return $clonedEntries;
    }

    protected function setupBulkClone3Routes($segment, $routeName, $controller)
    {
        Route::post($segment . '/bulk-clone/3', [
            'as' => $routeName . '.bulkClone3',
            'uses' => $controller . '@bulkClone3',
            'operation' => 'bulkClone3',
        ]);
    }

    protected function setupBulkClone3Defaults()
    {
        $this->crud->allowAccess('bulkClone3');

        $this->crud->operation('list', function () {
            $this->crud->enableBulkActions();
            $this->crud->addButtonFromView('bottom', 'bulk_update_3', 'bulk_update_3', 'bulk_update_3',);

        });
    }

    public function bulkClone4()
    {
        $this->crud->hasAccessOrFail('list');

        $entries = $this->crud->getRequest()->input('entries');
        $clonedEntries = [];

        foreach ($entries as $key => $id) {
            if ($entry = $this->crud->model->find($id)) {
                $clonedEntries[] = $entry->update(['stage_id' => 4]);
            }
        }
        return $clonedEntries;
    }

    protected function setupBulkClone4Routes($segment, $routeName, $controller)
    {
        Route::post($segment . '/bulk-clone/4', [
            'as' => $routeName . '.bulkClone4',
            'uses' => $controller . '@bulkClone4',
            'operation' => 'bulkClone4',
        ]);
    }

    protected function setupBulkClone4Defaults()
    {
        $this->crud->allowAccess('bulkClone4');

        $this->crud->operation('list', function () {
            $this->crud->enableBulkActions();
            $this->crud->addButtonFromView('bottom', 'bulk_update_4', 'bulk_update_4', 'bulk_update_4',);

        });
    }


}
