<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminMessageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AdminMessageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdminMessageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AdminMessage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/adminmessage');
        CRUD::setEntityNameStrings('Сообщение от админа', 'Сообщение от админа');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
//        CRUD::column('id');
        CRUD::column('user_id')->attribute('login')->label('Пользователь');
//        CRUD::column('notification_history_id');
        CRUD::column('message')->label('Сообщение');
//        CRUD::column('deleted_at');
        CRUD::column('weight')->label('Вес');
        CRUD::column('quantity')->label('Кол-во треков');
        CRUD::column('payment')->label('К оплате');
        CRUD::column('created_at')->label('Дата создания');
//        CRUD::column('updated_at');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AdminMessageRequest::class);

//        CRUD::field('id');
        CRUD::field('user_id')->attribute('login')->label('Пользователь');
//        CRUD::field('notification_history_id');
        CRUD::field('message')->label('Сообщение');
        $this->crud->addField([
            'name' => 'weight',
            'type' => 'text',
            'label' => 'Вес',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'name' => 'quantity',
            'type' => 'text',
            'label' => 'Кол-во треков',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'name' => 'payment',
            'type' => 'text',
            'label' => 'К оплате',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
//        CRUD::field('created_at');
//        CRUD::field('updated_at');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
