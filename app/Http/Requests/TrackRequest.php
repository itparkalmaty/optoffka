<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'track_number' => 'required|unique:ships',
            'track_number' => 'required',
            'description' => 'required',
            'stage_id' => 'required',
//            'user_id' => 'required'
        ];
    }
}
