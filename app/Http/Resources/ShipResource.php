<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        if ($this->date_of_ship != null){
            return [
                'id' => $this->id,
                'track_number' => $this->track_number,
                'description' => $this->description,
                'updated_at' => $this->date_of_ship->format('d.m.Y'),
                'stage' => new StageResource($this->stage),
            ];
        }else{
            return [
                'id' => $this->id,
                'track_number' => $this->track_number,
                'description' => $this->description,
                'updated_at' => $this->updated_at->format('d.m.Y'),
                'stage' => new StageResource($this->stage),
            ];
        }

    }
}
