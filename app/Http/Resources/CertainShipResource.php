<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CertainShipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'track_number' => $this->track_number,
            'description' => $this->description,
            'created_at' => $this->created_at->format('d.m.Y'),
//            'stage' => new StageResource($this->stage)
        ];
    }
}
