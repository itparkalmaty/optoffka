<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserTrackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'weight' => $this->weight,
            'quantity' => $this->quantity,
            'parish_sum' => $this->parish_sum,
            'date' => $this->date->format('d.m.Y'),
        ];
    }
}
