<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipStageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'track_number' => $this->track_number,
            'created_at' => $this->updated_at->format('d.m.Y'),
            'stage' => new StageResource($this->stage)
        ];
    }
}
