<?php


namespace App\Traits;


use Illuminate\Support\Facades\Http;

trait WebSocket
{
    public static function sendEvent($event, $data)
    {
        Http::post('https://socket.bhub.kz',[
            'event' => 'play-ads',
            'data' => $data
        ]);
    }
}
