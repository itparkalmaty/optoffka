<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TrackTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
//    public function test_example()
//    {
//        $response = $this->post('/api/create/track', [
//                'track_number' => '122453',
//                'description' => 'BehPt9dx1k6izhgaEgrcsORY6qv5tMynrTxAORpskC',
////                'stage_id' => 1,
//            ]
//            ,
//            [
//                'Authorization' => 'Bearer 5|0smoVwxEqVk2ARQPlWelPtejN7BmeafJJAUdJsOI'
//            ]
//        );
//        $response->decodeResponseJson();
//        $response->assertStatus(201);
//    }

    public function test_example()
    {
        $response = $this->get('/api/show/track',
//            [
//                'track_number' => '122453',
//                'description' => 'BehPt9dx1k6izhgaEgrcsORY6qv5tMynrTxAORpskC',
////                'stage_id' => 1,
//            ]
//            ,
            [
                'Authorization' => 'Bearer 6|QaFxmTywI4JjwSjQ4TfSOExwjZIlTb3QxXbfyX4Q'
            ]
        );
        $response->decodeResponseJson();
        $response->assertStatus(200);
    }
}
