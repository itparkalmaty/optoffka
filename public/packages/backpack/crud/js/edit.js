/*
*
* Backpack Crud / Edit
*
*/

jQuery(function ($) {

    'use strict';
});

document.addEventListener('DOMContentLoaded',()=>{
    setInterval(() => {
        let deleteButtons = document.querySelectorAll('.select2-selection__choice__remove')
        deleteButtons.forEach(deleteButton => {
            deleteButton.addEventListener('click', () => {
                console.log(deleteButton.parentElement)

                fetch(`/api/admin/unlink/${deleteButton.parentElement.getAttribute('title')}`)
                    .then(response=>console.log(response))
            })
        })


    }, 1000)


    let btn = document.querySelector('.btn-success')
    btn.addEventListener('click',(e)=>{
        let deleteButtons = document.querySelectorAll('.select2-selection__choice__remove')
        let userId = document.getElementsByClassName('form-control')[0].value
        deleteButtons.forEach(deleteButton=>{
            fetch(`/api/admin/link/${deleteButton.parentElement.getAttribute('title')}?user=${userId}`)
        })
    })

})



