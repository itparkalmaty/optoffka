<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('ship') }}'><i class='nav-icon la la-clipboard-list'></i> Трек номера</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon la la-user-alt'></i> Пользователи</a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('paymentstatus') }}'><i class='nav-icon la la-question'></i> Статусы оплаты</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('information') }}'><i class='nav-icon la la-exclamation-circle'></i> Информация</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class='nav-icon la la-object-group'></i> Баннеры</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('stage') }}'><i class='nav-icon la la-circle-o'></i> Этапы</a></li>
<li class='nav-item'><a class='nav-link' href='{{ route('uploads') }}'><i class='nav-icon la la-file-excel'></i> Выгрузка Excel</a></li>
<li class='nav-item'><a class='nav-link' href='{{ route('user-excel') }}'><i class='nav-icon la la-file-alt'></i> Файлы поступления</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('adminmessage') }}'><i class='nav-icon la la-mail-bulk'></i> Сообщение от админа</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('userinfo') }}'><i class='nav-icon la la-book'></i> Поступления пользователей</a></li>
