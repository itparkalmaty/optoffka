{{--@if ($crud->hasAccess('bulkClone') && $crud->get('list.bulkActions'))--}}
{{--    <a href="javascript:void(0)" onclick="bulkCloneEntries(this)" class="btn btn-sm btn-secondary bulk-button"><i class="fa fa-clone"></i> Отправки с Гуанчжоу</a>--}}
{{--@elseif($crud->hasAccess('bulkClone3') && $crud->get('list.bulkActions'))--}}
{{--    <a href="javascript:void(0)" onclick="bulkCloneEntries(this)" class="btn btn-sm btn-secondary bulk-button"><i class="fa fa-clone"></i> Приход в Алматы</a>--}}
@if($crud->hasAccess('bulkClone4') && $crud->get('list.bulkActions'))
    <a href="javascript:void(0)" onclick="bulkCloneEntries4(this)" class="btn btn-sm btn-secondary bulk-button"><i class="fa fa-clone"></i> Выданные клиентам</a>
@endif
@push('after_scripts')
    <script>
        if (typeof bulkCloneEntries4 != 'function') {
            function bulkCloneEntries4(button) {

                if (typeof crud.checkedItems === 'undefined' || crud.checkedItems.length == 0)
                {
                    new Noty({
                        type: "warning",
                        text: "<strong>{{ trans('backpack::crud.bulk_no_entries_selected_title') }}</strong><br>{{ trans('backpack::crud.bulk_no_entries_selected_message') }}"
                    }).show();

                    return;
                }

                var message = "Вы действительно хотите изменить статус :number треков на 'Выданные клиентам'?";
                message = message.replace(":number", crud.checkedItems.length);

                // show confirm message
                swal({
                    title: "{{ trans('backpack::base.warning') }}",
                    text: message,
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{{ trans('backpack::crud.cancel') }}",
                            value: null,
                            visible: true,
                            className: "bg-secondary",
                            closeModal: true,
                        },
                        delete: {
                            text: "Изменить",
                            value: true,
                            visible: true,
                            className: "bg-primary",
                        }
                    },
                }).then((value) => {
                    if (value) {
                        var ajax_calls = [];
                        var clone_route = "{{ url($crud->route) }}/bulk-clone/4";

                        // submit an AJAX delete call
                        $.ajax({
                            url: clone_route,
                            type: 'POST',
                            data: { entries: crud.checkedItems },
                            success: function(result) {
                                // Show an alert with the result
                                new Noty({
                                    type: "success",
                                    text: "<strong>Статус изменен</strong><br>Выданные клиентам "+crud.checkedItems.length+" треков."
                                }).show();

                                crud.checkedItems = [];
                                crud.table.ajax.reload();
                            },
                            error: function(result) {
                                // Show an alert with the result
                                new Noty({
                                    type: "danger",
                                    text: "<strong>Ошибка</strong><br>Произошла ошибка при изменении."
                                }).show();
                            }
                        });
                    }
                });
            }
        }
    </script>
@endpush
