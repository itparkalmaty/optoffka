@extends(backpack_view('blank'))
@section('content')
    <div class="column">
        <div class="col-md-6"
             style="background-color: #fff;padding: 15px;border-radius: 20px;box-shadow: 0 0 10px rgba(0,0,0,0.2);">
            <div class="mb-2">
                <h5>Закупки клиентов</h5>
            </div>
            <div class="" style="margin-top: 20px;border: 1px solid #ccc; padding: 10px;border-radius: 10px;">
                <input type="file" name="file" id="file">
                <input type="date" name="date" id="date">

            </div>
            <button class="btn btn-success" style="margin-top: 20px;" id="excel-import">
                Выгрузить
            </button>
        </div>
        <div class="col-md-12" id="message-box">

        </div>
    </div>

    <script>
        document.getElementById('excel-import').addEventListener('click', () => {
            let formData = new FormData()
            formData.append('booba', document.getElementById('file').files[0])
            formData.append('date', document.getElementById('date').value)
            fetch('/api/users/load', {
                method: 'POST',
                body: formData
            }).then(response => {
                response.json().then(data => document.getElementById('message-box').innerHTML = `<div class='alert-success ' style="margin-top: 20px; padding: 20px;font-size:40px;width: 65%;margin-left: auto;margin-right: auto; border-radius: 100%;text-align: center;"> ${data['message']} </div>`);
            }).catch(data => console.log(data));
        })
    </script>
@endsection
