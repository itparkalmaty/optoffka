{{--@extends(backpack_view('blank'))--}}
{{--@section('content')--}}


{{--    <section>--}}
{{--        --}}{{--        <span><h2>Коды товара: {{ $product->name }}</h2></span>--}}
{{--        <table class="table table-hover table-success">--}}
{{--            <thead class="bg-success">--}}
{{--            <tr>--}}
{{--                <th scope="col">Трек номер</th>--}}
{{--                <th scope="col">Дата создания</th>--}}
{{--                <th scope="col">Переход на 2 этап</th>--}}
{{--                <th scope="col">Переход на 3 этап</th>--}}
{{--                <th scope="col">Переход на 4 этап</th>--}}
{{--            </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--            @foreach($codes as $code)--}}
{{--                @if($code->user->id == backpack_user()->id || backpack_user()->hasRole('admin'))--}}
{{--                    <tr>--}}
{{--                        <th scope="row">{{ $ship->track_number }}</th>--}}
{{--                        <td>{{ $ship->created_at }}</td>--}}
{{--                        <td>{{ $ship->second_stage_date }}</td>--}}
{{--                        <td>{{ $ship->third_stage_date }}</td>--}}
{{--                        <td>{{ $ship->fourth_stage_date }}</td>--}}
{{--                        <td>--}}
{{--                            <a--}}
{{--                                href=""--}}
{{--                                class="btn btn-sm btn-link"><i class="la la-edit"></i> Редактировать</a>--}}
{{--                            @if($code->code_status_id != 4 && $code->code_status_id != 1)--}}
{{--                                <a--}}
{{--                                    href="{{ route('statusUpdate',$code) }}"--}}
{{--                                    class="btn btn-sm btn-link" style="color: red"><i class="la la-trash"></i> Снять с--}}
{{--                                    продажи--}}
{{--                                </a>--}}
{{--                            @endif--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                @endif--}}
{{--            @endforeach--}}

{{--            </tbody>--}}
{{--        </table>--}}
{{--            <div class="input-group mb-3">--}}
{{--                Нынешний статус--}}


{{--            </div>--}}
{{--    </section>--}}
{{--@endsection--}}

@extends(backpack_view('blank'))

{{--@php--}}
{{--    $defaultBreadcrumbs = [--}}
{{--      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),--}}
{{--      $crud->entity_name_plural => url($crud->route),--}}
{{--      trans('backpack::crud.preview') => false,--}}
{{--    ];--}}

{{--    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs--}}
{{--    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;--}}
{{--@endphp--}}

@section('header')
    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2>
{{--            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>--}}
{{--            <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small>--}}
{{--            @if ($crud->hasAccess('list'))--}}
{{--                <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>--}}
{{--            @endif--}}
        </h2>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="">

            <!-- Default box -->
            <div class="">
{{--                @if ($crud->model->translationEnabled())--}}
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <!-- Change translation button group -->
                            <div class="btn-group float-right">

                                <ul class="dropdown-menu">
{{--                                    @foreach ($crud->model->getAvailableLocales() as $key => $locale)--}}
{{--                                        <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>--}}
{{--                                    @endforeach--}}
                                </ul>
                            </div>
                        </div>
                    </div>
{{--                @endif--}}
                <div class="card no-padding no-border">
                    <table class="table table-striped mb-0">
                        <tbody>
{{--                        @foreach ($crud->columns() as $column)--}}
                            <tr>
                                <th scope="col">Трек номер</th>
                                <th scope="col">Нынешний статус</th>
                                <th scope="col">Дата создания</th>
                                <th scope="col">Гуанчжоу</th>
                                <th scope="col">Алматы</th>
                                <th scope="col">Выдано</th>
                            </tr>
{{--                        @endforeach--}}
{{--                        @if ($crud->buttons()->where('stack', 'line')->count())--}}
                            <tr>
{{--                                <td><strong>{{ trans('backpack::crud.actions') }}</strong></td>--}}
                                <th scope="row">{{ $ship->track_number }}</th>
                                <th scope="row">{{ $ship->stage->name }}</th>
                                <td>{{ $ship->created_at }}</td>
                                <td>{{ $ship->second_stage_date }}</td>
                                <td>{{ $ship->third_stage_date }}</td>
                                <td>{{ $ship->fourth_stage_date }}</td>
                            </tr>
                        </tbody>
                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
@endsection


@section('after_styles')
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css').'?v='.config('backpack.base.cachebusting_string') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css').'?v='.config('backpack.base.cachebusting_string') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('packages/backpack/crud/js/crud.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
@endsection
