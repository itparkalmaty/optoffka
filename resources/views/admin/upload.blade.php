@extends(backpack_view('blank'))
@section('content')
    <div class="column">
        <div class="col-md-6"
             style="background-color: #fff;padding: 15px;border-radius: 20px;box-shadow: 0 0 10px rgba(0,0,0,0.2);">
            <div class="mb-2">
                <h5>Отправки c Гуанчжоу</h5>
            </div>
            <div class="" style="margin-top: 20px;border: 1px solid #ccc; padding: 10px;border-radius: 10px;">
                <input type="file" name="file" id="file">
                <input type="date" name="date-1" id="date-1">
            </div>
            <button class="btn btn-success" style="margin-top: 20px;" id="excel-import-stage-1">
                Выгрузить
            </button>
        </div>
        <div class="col-md-6"
             style="margin-top: 2% ;background-color: #fff;padding: 15px;border-radius: 20px;box-shadow: 0 0 10px rgba(0,0,0,0.2);">
            <div class="mb-2">
                <h5>Приход в Алматы</h5>
            </div>
            <div class="" style="margin-top: 20px;border: 1px solid #ccc; padding: 10px;border-radius: 10px;">
                <input type="file" name="file-2" id="file-2">
                <input type="date" name="date-2" id="date-2">

            </div>
            <button class="btn btn-success" style="margin-top: 20px;" id="excel-import-stage-3">
                Выгрузить
            </button>
        </div>

        <div class="col-md-6"
             style="margin-top: 2%;background-color: #fff;padding: 15px;border-radius: 20px;box-shadow: 0 0 10px rgba(0,0,0,0.2);">
            <div class="mb-2">
                <h5>Выданные клиентам</h5>
            </div>
            <div class="" style="margin-top: 20px;border: 1px solid #ccc; padding: 10px;border-radius: 10px;">
                <input type="file" name="file-3" id="file-3">
                <input type="date" name="date-3" id="date-3">
            </div>
            <button class="btn btn-success" style="margin-top: 20px;" id="excel-import-stage-4">
                Выгрузить
            </button>
        </div>


        <div class="col-md-12" id="message-box">

        </div>
    </div>

    <script>
        document.getElementById('excel-import-stage-1').addEventListener('click', () => {
            let formData = new FormData()
            formData.append('booba', document.getElementById('file').files[0])
            formData.append('date_of_ship', document.getElementById('date-1').value)
            fetch('/api/load', {
                method: 'POST',
                body: formData
            }).then(response => {
                response.json().then(data => document.getElementById('message-box').innerHTML = `<div class='alert-success ' style="margin-top: 20px; padding: 20px;font-size:40px;width: 65%;margin-left: auto;margin-right: auto; border-radius: 100%;text-align: center;"> ${data['message']} </div>`);
            }).catch(data => console.log(data));
        })

        document.getElementById('excel-import-stage-3').addEventListener('click', () => {
            let formData = new FormData()
            formData.append('dooba', document.getElementById('file-2').files[0])
            formData.append('date_of_ship', document.getElementById('date-2').value)
            fetch('/api/other/load', {
                method: 'POST',
                body: formData
            }).then(response => {
                response.json().then(data => document.getElementById('message-box').innerHTML = `<div class='alert-success ' style="margin-top: 20px; padding: 20px;font-size:40px;width: 80%;margin-left: auto;margin-right: auto; border-radius: 100%;text-align: center;"> ${data['message']} </div>`);
            }).catch(data => console.log(data));
        })

        document.getElementById('excel-import-stage-4').addEventListener('click', () => {
            let formData = new FormData()
            formData.append('pooba', document.getElementById('file-3').files[0])
            formData.append('date_of_ship', document.getElementById('date-3').value)
            fetch('/api/another/load', {
                method: 'POST',
                body: formData
            }).then(response => {
                response.json().then(data => document.getElementById('message-box').innerHTML = `<div class='alert-success ' style="margin-top: 20px; padding: 20px;font-size:40px;width: 80%;margin-left: auto;margin-right: auto; border-radius: 100%;text-align: center;"> ${data['message']} </div>`);
            }).catch(data => console.log(data));
        })


    </script>
@endsection
